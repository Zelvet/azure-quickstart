resource "azurerm_subnet" "SUBNET1" {
    type = map(any)
    default = {
        name = var.SUBNET1.name
        address_prefix = [var.SUBNET1.address_prefix]
    }  
}